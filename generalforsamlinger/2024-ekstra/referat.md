# data.coop Ekstraordinær Generalforsamling 15. maj 2024

## Tid og sted

Internettet, kl 19:00

## Dagsorden

1. Valg af dirigent og referent.
2. Fremlæggelse og diskussion af Moderationspolitik
3. Beslutning om Moderationspolitik

## Referat

Til stede: Mikkel, Daniel, Fedder, Karsten, Klaus Slott, Martin (niemadsen), Sam A., Thomas Hansen, Balder, Halfdan.

### 1. Valg af dirigent og referent

Der er blevet budt velkommen.

Der er valgt dirigent og referent.

Balder er dirigent og Halfdan er referent.

Indkaldelsen er udsendt rettidigt.

### 2. Fremlæggelse og diskussion af Moderationspolitik

Første punkt er "Forslag til Mastodon moderationspolitik og håndbog for moderatorer".

Forslaget til en Moderationspolitik kan læses her: https://git.data.coop/data.coop/dokumenter/pulls/28

data.coop startede en Mastodon instans, men har manglet en moderationspolitik.

Vi har lagt os op af vores acceptable usage policy, men den har mangler i forhold til at køre en social media service, derfor har vi i det sidste år arbejdet på en moderationspolitik.

Den potentielle moderationspolitik er blevet fremlagt og der bliver nu spurgt ud i forsamlingen om der er kommentarer til forslaget.

For at komme med ændringsforslag, bedes der om at lave pull requests.

Det præciseres at forslag skal være indsendt en uge før næste generelforsamling.

Der bliver spurgt ind til hvordan processen med at blokere en instans foregår.

Umiddelbart vil det være en åben process.

Der bliver snakket om hvor grænser for hvornår en blokering af enkeltpersoner foregår.

Det vil nok være individuelle vurderinger.

Det er nemmere på vores egen instans, fordi vi kan læne os op af vores AUP.

Det bliver foreslået at vi giver moderator-teamet ret vide beføjelser til at bruge deres sunde fornuft.

Det gøres klart at man altid selv kan blokere/mute en bruger, hvis der er nogen man ikke gider at høre på.

Der bliver snakket mere specifikt om federering med Meta/Threads.

Et argument er at Meta/Threads forretning er fundamentalt anderledes fra data.coops og der derfor bør blokeres. Der er rigeligt med andre instanser man kan være på hvis man vil kommunikere med brugere på den platform.

Der bliver snakket om at blokere dem af idialistiske/aktivistiske årsager. At sige "I er ikke velkomne her, I har ikke arbejdet for at lave et godt internet, de sidste 10-15 år"

Der bliver snakket videre omkring emnet.

Der bliver spurgt ind til hvorvidt moderations-teamet kan overskue opgaven.

Det har været fint at moderere det seneste år, delvist fordi vi måske bliver skærmet en del, ved at federere med instanser der i forvejen blokerer for en del.

Der har været et par bølger med spam, men ikke i en større grad.

[Mikkel overtager referatet, da Halfdan skal smutte]

Der bliver snakket lidt meta-snak om ændringer til forslag/pull requests efter de er sendt ud.

De småændringer (formatering, slåfejl) der var lagt til forslaget efter deadline er trukket tilbage og vil blive fremsat som forslag på den ordinære generalforsamling.

### 3. Beslutning om Moderationspolitik

Afstemning om forslaget:

8 stemmer for. 0 imod. Forslaget er vedtaget.
