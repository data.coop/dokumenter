# Bestyrelsens beretning 2023-2024


## Hvad er der sket?
Hvis man skal beskrive perioden siden sidste generalforsamling, i data.coop sammenhænge, så kan man vist kun tale i positive vendinger.

- Vi har fået indkøbt harddiske og sat dem i "server 2", og arbejdet med at konsolidere serverne er i fuld gang.
- Vi har skiftet bank og er nu hos Arbejdernes Landsbank hvor vi har markant færre omkostninger end hos vores forrige bank.
- Der kommer flere og flere medlemmer til og foreningen, og vi tæller nu hele 50 betalende medlemmer!


## Medlemsydelse

Bestyrelsen vurderer det er på tide til at vi tager næste skridt, og derfor har vi udarbejdet et forslag der omhandler at indføre en medlemsydelse for alle medlemmer. Medlemsydelsen skal bruges til at styrke foreningens økonomi og give os bedre mulighed for at kunne erstatte defekt hardware, eller endda investere i ny hardware for at udvide foreningens virke (bare rolig, vi pønser ikke på ai.data.coop).


## Mere netværk

Vi har mødt Kooperativt København og snakket om synergier. Vi foreslår at blive medlem med alles opbakning og informerer om de spændende arrangementer i netværket.

Vi holdt et oplæg hos Kritik Digital om data.coop og er klar med slides til flere lignende oplæg.

Vi inviterede Paris Marx og fik stødte fra IDA og PROSA. Det blev et rigtig godt besøg og viser, at data.coop kan række bredt ud i sit virke.

På Bornhack mødte vi flere nye medlemmer i 2023 og gør det igen i 2024.


## Medlemssystemet

Det er desværre ved at blive en stående joke at vores medlemssystem bliver klar "snart". Desværre er det ikke sket de helt store fremskridt som vi har håbet på. Dog er vores visuelle identitet blivet implementeret og grundstene for at bruge medlemssystemet som single sign-on er blevet lagt.

Vores håb er at vi inden næste generalforsamling har et medlemsystem som bliver indgangsvinklen til vores tjenester.


## Moderation af fødivers-tjenester

På sidste generalforsamling var der et forslag om at introducere en moderationspolitik for tjenester der er på fødiverset. Forslaget blev, grundet nogle teknikaliteter, trukket tilbage og en arbejdsgruppe der skulle behandle forslaget blev oprettet.

Gruppens arbejde blev endeligt vedtaget ved en ekstraordinær generalforsamling d. 15. maj 2024.


## Hej!-dage

Vi har afholdt "hej!"-dage med relativt stor succes. Det er vores indtryk at det giver et godt sammenhold i foreningen og vi håber at vi kan skrue op for aktiviteter hvor vi som medlemmer samles, nørder og diskuterer teknologi.


## Tjenester

De følgende tjenester er føjet til siden sidste generalforsamling:

- https://status.data.coop: For at have bedre indsigt i vores tjenesters oppetid har vi introduceret status.data.coop. Denne er placeret på en anden server end den som tjenesterne kører på.
- https://write.data.coop/: En writefreely instans, som er en blogging platform i fødiverset.

Desuden er vores ansible konfiguration blevet omskrevet på en måde der gør vedligeholdelse af eksisterende og oprettelsen af nye tjenester nemmere. Stor tak til administratorholdet for at holde vores infrastruktur opdateret!

Alt i alt ser det lyst ud for data.coop. Nu er det bare om at holde skruen i vandet og fortsætte den flotte kurs! ⛴️
