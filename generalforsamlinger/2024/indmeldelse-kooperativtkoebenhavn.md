# Medlemskab af Kooperativt København

## Motivation

Det foreslås til Generalforsamlingen 2024s beslutningsprotokol, at vi indmelder os i [Kooperativt København](https://kooperativtkoebenhavn.dk/).

Vi ønsker hele foreningens medvidende og opbakning til beslutningen, eftersom:

* Vi ønsker at befinde os i et fællesskab af andre slags kooperativer, som vi opfatter som ligesindede og fremtidige alliancer.

* Kooperativt København afholder medlemsaktiviteter, som vil være åbne for alle data.coops medlemmer.

* Vi har drøftet forskellige samarbejdspotentialer med Kooperativt København, herunder at stille os til rådighed for at uddanne og videndele om digitale kooperativer platforme i Kooperativt København.

* Kooperativt Københavns medlemmer er meget vel interesserede i at kunne købe ydelser hos andre kooperativer. Derfor kan data.coop overveje, om vi vil udvikle os i den retning.

## Beslutning

Bestyrelsen finder og deler i øvrigt vedtægterne og tilser, at de stemmer overens med data.coops formål og interesser.

Data.coop retter henvendelse til Kooperativt København om at blive optaget som medlem.

Indeværende år er gratis, herefter koster medlemskabet 1000 kr årligt.

Bestyrelsen modtager herefter oplysninger fra Kooperativt København og deler oplysninger om medlemsaktiviteter med resten af data.coop.
