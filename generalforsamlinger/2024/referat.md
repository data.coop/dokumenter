# Referat af Generalforsamling 2024


## Tid og sted

Internettet, 1. juni 2024

## Deltagere

Brug fulde navne, med mindre folk ønsker anonymiseret alias.

- Vidir Valberg Gudmundsson
- Mikkel Munch Mortensen
- Reynir Björnsson
- Wilian Juul G-H
- Karsten Sandmand :star2: 
- Benjamin Balder Bach
- Daniel N
- Klaus Vink Slott
- Thomas Hansen
- Niels Jensen
- Morten Sørensen
- Martin Niemann Madsen

## Dagsorden (generalforsamling)

1. Valg af dirigent og referent.
1. Bestyrelsens beretning.
1. Fremlæggelse af regnskab, budget og kontingent.
1. Indkomne forslag.
1. Godkendelse af vedtægtsændringer og Acceptable Use Policy
1. Valg (Jf. § 3)
1. Eventuelt

### 1. Valg af dirigent og referent.

* Mikkel er referent
* Vidir er dirigent

Det konstateres, at forsamlingen er lovligt indkaldt.

### 2. Bestyrelsens beretning.

[Beretningen kan findes her](https://git.data.coop/data.coop/dokumenter/src/branch/main/generalforsamlinger/2024/bestyrelsens_beretning.md)

Klaus spørger om foreningen har overvejet at tilbyde mail service til medlemmer.

Vidir forklarer at vi _har_ en mailserver kørende og man kan godt få en @data.coop e-mail-adresse. Eget domæne er ikke understøttet endnu, men er på tegnebrættet.


### 3. Fremlæggelse af regnskab, budget og kontingent.

#### Regnskab

Balder fremlægger regnskab og underholder om glæderne ved moms.

Det er værd at bemærke at vi havde et stort overskud, da vi ikke nåede at bruge vores donation til diske indenfor samme regnskabsår '23. Det forventes derfor et ca. tilsvarende underskud i år '24.

Daniel spørger om der laves revision på regnskabet. Vi har kun intern revision i form af Karsten, som siger han har fulgt med. Balder mener ikke det giver mening at have ekstern revision endnu i vores tilfælde, da beløbene stadig er relativt små.

Regnskabet blev godkendt enstemmigt (selvom det efter gældende vedtægter ikke er et krav).


#### Budget

Balder fremlægger budgetforslag 1.

Han forklarer hvorfor vi efterhånden er modne nok til at dele indbetaling til medlemskabet i kontingent og ydelse.

De 3 budgetforslag fremlægges og det forklares, hvorfor det kan give mening at hæve den samlede indbetaling pr. medlem, for at polstre os til eventuelle udgifter til ny hardware.

Reynir konstaterer at der ikke er noget om nedsat kontingent/medlemsydelse i budgetforslagene. Vidir forklarer, at intentionen var, at det skulle være henholdsvist 50 kr. og 50 kr. (inkl. moms). Balder liveopdaterer budgettet. Vidir bemærker, at muligheden for nedsat betaling bestemt fortsat skal være en mulighed.

Vidir anbefaler forslag nr. 3. Det vil være godt hvis vi har råd til at have begge servere kørende. Og evt. en dag også have hardware kørende et andet sted end Tyklings datacenter.

Balder synes det er godt at der er sat penge af til hej-dage.

Reynir synes det været et godt selling point at det kun har kostet 300 kr. om året at være medlem. Men han er samtidigt også med på at hæve det, for at sikre bæredygtigheden af projektet.

Daniel ønsker, at finde konsensus om hvilken model vi vælger. Måske en anonym afstemning.

Wilian spørger til gradueret betaling, afhængigt af hvilke ydelser man rent faktisk modtager. Balder siger at det er tanker vi allerede har, men at vi ikke helt er nået til at kunne administrere det endnu.

Balder forklarer at det fra bestyrelsens side i første omgang bare er intentionen at vi får introduceret en særskilt betaling for ydelse.

Vidir har fundet en online afstemning som vi benytter til at finde ud af om der er opbakning til forslag 3. Det er enstemmigt vedtaget, og bekræftet via håndsoprækning i chatten.

### 4. Indkomne forslag.


#### [Vedtægtsændring: Medlemsydelse](https://git.data.coop/data.coop/dokumenter/pulls/43)

Vidir motiverer forslaget.

Daniel spørger om det er korrekt forstået at det er bestyrelsen der fastsætter ydelsessatserne. Det er det. Det kan man have teoretiske forbehold for. Men der er netop også skrevet ind at hvis en tilpas stor del af medlemmerne er imod hvad der foregår, skal der indkaldes til en ekstraordinær generalforsamling om emnet.

Forslaget er enstemmigt vedtaget.

#### [Vedtægtsændring: Separat afsnit for Tegningsret + ny regel for prokura](https://git.data.coop/data.coop/dokumenter/pulls/35)

Balder motiverer.

Forslaget er enstemmigt vedtaget.

#### [Vedtægtsændring: Specificering af formalia i dagsordnen](https://git.data.coop/data.coop/dokumenter/pulls/36)

Vidir motiverer.

Forslaget er enstemmigt vedtaget.


#### [Vedtægtsændring: Præcisér at regnskab, budget og kontingent skal godkendes.](https://git.data.coop/data.coop/dokumenter/pulls/46)

Vidir motiverer.

Forslaget er enstemmigt vedtaget.

#### [Vedtægtsændring: Fjern punkt 5. fra dagsorden.](https://git.data.coop/data.coop/dokumenter/pulls/45)

Vidir motiverer.

Forslaget er enstemmigt vedtaget.

#### [Vedtægtsændring: Tilføj paragraf om moderationspolitik](https://git.data.coop/data.coop/dokumenter/pulls/47)

Vidir og Balder motiverer.

Daniel synes, det er et fornuftigt forslag. Man kan spørge, om der er dele af moderationspolitikken, som det kun er bestyrelsen der kan håndhæve. Svar: Det er der iht. vedtagne Moderationspolitik ifm. defederering af andre servere i fediverset.

Reynir spørger til processen for at udtrykke mistillid til moderatorer. Det er er en følsom rolle. Det er ikke nævnt, men Vidir foreslår at vi laver et ændringsforslag til forslaget for at få det med.

Der holdes en kort pause mens forslaget rettes til, så vi også har en nedskrevet proces for hvordan mistillid til moderatorer håndteres.

Ændringsforslag 1: [GF2024: Ændringsforslag til "Vedtægtsændring: Tilføj paragraf om moderationspolitik": Tilføjelse af mistillidsparagraf](https://git.data.coop/data.coop/dokumenter/pulls/51)

Ændringsforslaget er enstemmigt vedtaget.

Forslaget er enstemmigt vedtaget.

#### [Smårettelser til Moderationspolitik](https://git.data.coop/data.coop/dokumenter/pulls/42)

Balder motiverer. Det er korrekturrettelser.

Forslaget er enstemmigt vedtaget.

#### [AUP-ændring: Omformuler fra specifikke kanaler til noget mere generelt](https://git.data.coop/data.coop/dokumenter/pulls/41)

Mikkel motiverer.

Forslaget er vedtaget. Bortset fra en enkelt blank stemme, stemte alle for.

#### [Forslag om indmeldelse i Kooperativt København](https://git.data.coop/data.coop/dokumenter/pulls/48)

Balder motiverer.

Daniel spørger om vi har adgang til Kooperativt Københavns vedtægter. Han har ikke selv kunnet finde dem. Svaret er "Næh". Daniel synes det er interessant at læse inden vi melder os ind. Mikkel foreslår at vi skaffer dem, deler dem og så kan medlemmer melde tilbage hvis der er noget i det man ikke kan være med til.

Klaus spørger til "København"-delen af det. Er data.coop Københavnerfokuseret? Svaret er klart nej. Vores eneste forbehold er at vi på et tidspunkt vil nå en størrelse hvor vi ikke ønsker at være flere. Reynir læser i vores vedtægter at:

> Foreningens hjemsted er Københavns Kommune, men primært internettet.

Balder og Mikkel var til et positivt møde med Kooperativt København og deres ideer går meget langt ud over København.

Forslaget er enstemmigt vedtaget.

### 5. Godkendelse af vedtægtsændringer og Acceptable Use Policy.

Er klaret i ovenstående.

### 6. Valg (Jf. § 3).

Den nuværende bestyrelse består af:

- Bestyrelsesmedlemmer:
    - Balder
    - Halfdan
    - Mikkel
    - Reynir (på valg)
    - Vidir (på valg)
- Suppleanter (på valg):
    - Sam
    - Sune

Vidir og Reynir genopstiller begge til bestyrelsen. Der er ikke andre kandidater. De er hermed genvalgt.

Vidir indstiller Sam in absentia. Daniel stiller også op. De er begge valgt.

### 7. Eventuelt.

Intet til eventuelt.

Vidir takker for god ro og orden.