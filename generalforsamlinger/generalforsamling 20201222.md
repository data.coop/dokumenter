# Generalforsamling 22/12 2020

## Referat af Generalforsamling i data.coop 2020

Tirsdag d. 22. december 2020 kl 20
Online på jitsi og IRC/Matrix

### Deltagere

Mikkel Munch-Mortensen (decibyte)   
Benjamin Bach (benjaoming)  
Jesper Hess Nielsen (graffen)  
Reynir Björnsson (reynir)  
Thomas Bødtcher-Hansen  
Thomas Laumann  
Víðir Valberg Gudmundsson (valberg) (AFK)  
Nicolai Søborg  
Robo  
Karsten Højgaard (under Evt.)  
Haris (gæst)

### Dagsorden

1. Valg af dirigent og referent.
1. Bestyrelsens beretning.
1. Fremlæggelse af regnskab, budget og kontingent.
1. Indkomne forslag.
1. Godkendelse af vedtægtsændringer og Acceptable Use Policy
1. Valg (Jf. § 3)
1. Eventuelt

#### Valg af dirigent og referent

valberg blev valgt som dirigent og reynir referent.

```
2020-12-22 20:05:46	@reynir	Til referatet: der er forvirring om hvilken side er benjaoming's højre og venstre
2020-12-22 20:09:00	@benjaoming	reynir: er valgt til dirigent
2020-12-22 20:09:06	@reynir	reynir er referent, og valberg er dirigent
```

#### Bestyrelsens beretning

valberg giver bestyrelsens beretning.

```
2020-12-22 20:11:21	@reynir	valberg giver bestyrelsens beretning. Der er ikke sket meget, men alligevel noget. Serveren kører, arbejdsdage.
2020-12-22 20:11:30	 *	hacuko[m] sent a long message:  < https://matrix.org/_matrix/media/r0/download/matrix.org/YqewYjexVxTViMwctGmQPZmj/message.txt >
2020-12-22 20:12:07	graffen[m]	hacuko: Prøv evt. i chromium
2020-12-22 20:12:35	@reynir	Vi har test hjemmeside og klistermærker med vores nye identitet
2020-12-22 20:12:52	@reynir	https://new.data.coop/
2020-12-22 20:13:05	hacuko[m]	<graffen[m] "hacuko: Prøv evt. i chromium"> tak.. men har kun firefox pt. på den her maskine.. prøver lige various fixes.. :)
2020-12-22 20:13:14	@reynir	Der blev udarbejdet en acceptable usage policy (AUP) af haris
2020-12-22 20:15:28	@reynir	Der bliver snakket om medlemssystem, og at det vil være fantastisk at få op at køre i 2021. Noget pragmatisk i første omgang. TODO på arbejdsdag.
```

I løbet af det seneste år er der ikke sket meget men alligevel noget.
Vores server kører fortsat og vi har haft et par arbejdsdage.
Klistermærker med data.coops nye identitet er blevet lavet og det samme for en test-hjemmeside https://new.data.coop/.
Haris var i praktik og fik udarbejdet den fine Acceptable Usage Policy (AUP) der skal godkendes til generalforsamlingen.

#### Fremlæggelse af regnskab, budget og kontingent

Regnskabet for 2019 fremlægges.
Det er præget af en bank der er dyr i gebyrer.
Alternative banker undersøges igen.
Regnskabet 2019 godkendes.

```
2020-12-22 20:17:24	@reynir	Vi fortsætter til pkt. 3 Fremlæggelse af regnskab, budget og kontingent. Balder tager over
2020-12-22 20:18:06	@reynir	Regnskab for 2019 bliver fremlagt
2020-12-22 20:18:43	@reynir	Vi har en dyr bank, og det vil vi gerne gøre noget ved. Regnskabet er 1:1 med bankkonto ind/ud
2020-12-22 20:21:25	@reynir	Vi bruger mange penge for bankkonti i Merkur. Vi har været glade der, men det er blevet meget dyrt nu da gebyrerne har steget og steget.
2020-12-22 20:26:43	@reynir	Vi har undersøgt alternative banker før, og der er ikke umiddelbart bedre priser som ikke er Nordea eller Danske Bank. Vi skal undersøge Folkesparekassen igen, der var forvirring med Middelfart Sparekasse
2020-12-22 20:26:57	@reynir	Vi har 16 gyldige medlemmer pt.
2020-12-22 20:27:16	@reynir	(efter sigende, benjaoming sender præcise tal senere)
2020-12-22 20:28:10	@reynir	Regnskabet for 2019 er godkendt af generalforsamlingen
```

Minimumskontingent á 300 kr. som ved sidste generalforsamling godkendes.
Det koster 300 kr. pr. år at være medlem, men man må meget gerne betale mere som en slags donation for at dække driftsomkostninger.

```
2020-12-22 20:37:59	@reynir	Minimumskontingent á 300 kr er godkendt af generalforsamlingen efter en snak om at minimumskontingent gør det muligt at donere ekstra penge.
2020-12-22 20:38:42	@reynir	Der laves live coding af data.coop budget 2021
2020-12-22 20:38:56	@reynir	Imens fortæller valberg Fine Jokes™
2020-12-22 20:41:49	@reynir	Der foreslåes en ny crowdsourcet service: puns.data.coop
```

benjaoming udarbejder et budget for 2021 live som godkendes.
<!-- FIXME: nogle detaljer om budgettet?! -->

```
2020-12-22 20:49:55	@reynir	Budget for 2021 godkendes
```

#### Indkomne forslag

Ingen forslag blev modtaget rettidigt.

#### Godkendelse af vedtægtsændringer og Acceptable Use Policy

Acceptable Usage Policy bliver præsenteret af Haris og benjaoming.
Haris får et stort bifald for hans arbejde.
Policien bliver godkendt og hjemmesiden rettet så man kan finde AUP'en.

```
2020-12-22 20:51:55	@reynir	Vi er nået til punktet godkendelse af vedtægtsændringer og acceptable usage policy. Haris og benjaoming præsenterer AUP'en.
2020-12-22 20:53:23	@reynir	Stort bifald for haris' arbejde
2020-12-22 20:56:48	@reynir	AUP-forslaget kan læses her: https://git.data.coop/data.coop/dokumenter/pulls/7 
2020-12-22 21:11:04	@reynir	Der diskuteres ivrigt hvordan AUP skal versioneres
2020-12-22 21:12:20	@reynir	Der laves kompromis: første version hedder 1.0.0
2020-12-22 21:17:42	--	Notice(datacoop_buildbo): [[data.coop/website](https://git.data.coop/data.coop/website)] Pull request opened: [#22 Tilføj link til vedtægter og AUP i blivmedlem](https://git.data.coop/data.coop/website/pulls/22) by [reynir](https://git.data.coop/reynir)
2020-12-22 21:19:17	--	Notice(datacoop_buildbo): [[data.coop/dokumenter](https://git.data.coop/data.coop/dokumenter)] Pull request merged: [#7 Acceptable Usage Policy til Generalforsamling 2020](https://git.data.coop/data.coop/dokumenter/pulls/7) by [benjaoming](https://git.data.coop/benjaoming)
2020-12-22 21:19:44	@reynir	Pull requestet med AUP'en blev merget! Som den fine datacoop_buildbo kan rapporterer.
2020-12-22 21:20:53	--	Notice(datacoop_buildbo): Build success [data.coop/website#e0582e67](https://drone.data.coop/data.coop/website/71) (aup) by reynir
2020-12-22 21:20:54	--	Notice(datacoop_buildbo): Build success [data.coop/website#e0582e67](https://drone.data.coop/data.coop/website/72) (master) by reynir
2020-12-22 21:21:11	@reynir	mmm
```

#### Valg (Jf. § 3)

Alle blev genvalgt!

```
2020-12-22 21:21:11	@reynir	3 bestyrelsesposter er på valg, pt. besat af reynir, graffen og valberg. Derudover skal 2 suppleanter vælges (pt. Karsten og Tux), samt revisor (reynir) og evt. revisorsuppleant.
2020-12-22 21:22:58	@reynir	reynir, graffen og valberg blev genvalgt i bestyrelsen
2020-12-22 21:23:35	@reynir	De to suppleanter blev genvalgt
2020-12-22 21:25:27	@reynir	reynir blev genvalgt som revisor efter lidt tøven
```

#### Eventuelt

reynir præsenterer projekt med midler fra EUs NGI Pointer fond om deployment af [MirageOS](https://mirage.io/).
Der lyder opbakning på Jitsi.

```
2020-12-22 21:26:28	@benjaoming	Vi er nu nået til Eventuelt
2020-12-22 21:30:58	@reynir	reynir agiterede for at vi kører Mirage. Hos robur har vi fået midler fra EU til deployment af Mirage, så jeg kan bruge arbejdstid på data.coop \o/
```

benjaoming og Haris forklarer at praktik er en mulighed for andre.

```
2020-12-22 21:31:33	@reynir	benjaoming og haris snakkede om praktik, og at det er en mulighed for andre
```

valberg præsenterer en vedtægtsændringsforeslag om formaliseret administratorhold.
Vedtægsændringsforslaget blev ikke lavet i tide og kan derfor ikke godkendes til generalforsamlingen.
data.coop har haft administratorer som ikke er blevet valgt, og på generalforsamlingen bliver de genvalgt som de facto administratorer indtil vedtægsændringen kan blive vedtaget.

```
2020-12-22 21:32:34	@reynir	valberg præsenterer en vedtægtsændring der ikke kom i hænde i tide: https://git.data.coop/data.coop/dokumenter/pulls/5
2020-12-22 21:36:45	--	Notice(datacoop_buildbo): [[data.coop/dokumenter](https://git.data.coop/data.coop/dokumenter)] Pull request opened: [#8 AUP: Fix link to https://data.coop/tjenester](https://git.data.coop/data.coop/dokumenter/pulls/8) by [laumann](https://git.data.coop/laumann)
2020-12-22 21:45:58	@reynir	Vi blev enige om at fortsætte med graffen, valberg og reynir som de facto administrator indtil ændringen bliver vedtaget.
```
