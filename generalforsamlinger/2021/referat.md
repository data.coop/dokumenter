# Referat fra generalforsamling i data.coop, 24. marts 2021

11 er mødt op til vores virtuelle GF: Allan, Jesper, Thomas, Vidir, Karsten, Balder, Reynir, Jacob, Rasmus, Halfdan, Mikkel.


## 1: Valg af dirigent og referant

Jesper er valgt som dirigent. Mikkel som referant.


## 2: Bestyrelsens beretning

Forpersonen, Vidir, gennemgår [bestyrelsens beretning](https://git.data.coop/data.coop/dokumenter/src/branch/master/generalforsamlinger/2021/bestyrelsens%20beretning.md).

Beretningen er godkendt af GF.

## 3: Fremlæggelse af [regnskab](https://git.data.coop/data.coop/dokumenter/src/branch/master/generalforsamlinger/2021/data.coop%20regnskab%202020.pdf), [budget](https://git.data.coop/data.coop/dokumenter/src/branch/master/generalforsamlinger/2021/data.coop%20budget%202021.pdf) og kontingent

### Regnskab

En bemærkning til både regnskab og budget: Der er ikke nogen "balance" angivet eksplicit. Foreningen har altid kørt HELE sin økonomi gennem bankkontoen. Derfor har indestående i banken altid svaret til både aktiver og (hhv. bankkonto og egenkapital). I fremtidige fremlæggelser af regnskaber bør vi måske notere dette mere eksplicit.

Det viser sig at TykTech, vores hosting, har eftergivet tidligere års udeståender. Så vi skylder ikke længere dette.

Regnskabet er godkendt.


### Budget

Kassereren gør opmærksom på at vi ender med at have ca. 7000 kr på kontoen når året er omme. Og at disse penge måske kan bruges på noget konstruktivt.

Balder foreslår fx at bruge penge på nogle klistermærker eller andet merchandise, som der bakkes op om fra Vidir.

Halfdan spørger til om vi har en plan for hvor meget vi skal have stående som reserve.

Der teoretiseres over at vi måske en dag får brug for at udskifte servere, af den ene eller den anden grund. Og at det selvfølgelig vil være en god ide at have nogle penge klar til at bruge på det.

Jacob spørger til diskkapacitet ift. fx Nextcloud, hvor man kan risikere at folk hurtigt fylder på. Jesper siger det er en god ide at lave en plan for hvordan vi administrerer diskplads, bl.a. for hvordan og hvornår vi udvider.

Vidir siger at GF bør pådutte bestyrelsen at lægge en plan for både økonomisk og datamæssig reserve.

Der spørges til indsigt i hvilken hardware vi rent faktisk har pt. Dette vil der blive informeret om efter GF.

Budgettet er godkendt.


## 4: Indkomne forslag

Da begge er forslag til vedtægtsændringer, og der ikke er andre forslag, slår vi dette punkt sammen med pkt. 5 jf. standarddagsordenen.

### 4.1: [Præcisering af hvor længe et medlemskab varer](https://git.data.coop/data.coop/dokumenter/pulls/11)

Der spørges ind til hvad proceduren er for medlemmer der ikke betaler deres kontigent.

Mikkel foreslår at lave en procedure med 2 trin: Først lukkes for adgang og medlemmet adviceres. Herefter slettes data efter en fastlagt periode, hvis der ikke er betalt kontingent.

Det diskuteres om ikke det bør specificeres yderligere hvad processen er omkring betaling af kontingent (hvornår ift GF), varsler for blokering og sletning.

Mikkel foreslår at godkendte forslaget som det foreligger, og så lade nogen på en arbejdsdag arbejde med et forslag til at præcisere.

Balder har, i rollen som kasserer, også savnet en klarere procedure for kontingentbetalinger. Det har været ret rodet at sidde med.

Medlemmer der allerede i løbet af 2021 har betalt kontingent, behøver ikke betale igen efter denne GF: Deres medlemskab gælder for resten af 2021.

Forslaget er vedtaget med en enkelt stemme imod.

### 4.2: [Tilføjelse af paragraf om udpegning af administratorer](https://git.data.coop/data.coop/dokumenter/pulls/5)

Dette kommer på baggrund af et ønske på sidste GF om at formalisere hvem der har administratoradgang til systemerne.

Det uddybes at administratorrollen er for hele serverparken -- ikke for enkelte tjenester. Der findes altså kun 1 slags administrator, men der er flere af dem.

Halfdan siger det er dejligt det er et hold, så der ikke er nogen der sidder med ansvaret alene. Og så folk der kan noget, men ikke nødvendigvis alt, også kan være med.

Mikkel siger der også skal være plads til at melde sig på holdet mhp. at blive lært op.

Vidir siger at det heller ikke skal ende med at den tid man bruger på administratorrollen går med undervisning af andre.

Halfdan og Vidir snakker om dokumentation af de administrative processer.

Forslaget er vedtaget.

## 4.3: Acceptable Use Policy

Der er ingen ændringsforslag til AUP.

## 6: Valg

Følgende ønsker genvalg til bestyrelsen:

 * Balder
 * Mikkel

Balder melder dog ud at han gerne trækker sit kandidatur, hvis andre har lyst. Halfdan melder sig som kandidat.

Halfdan og Mikkel er valgt.

Til revisor opstiller:
 * Balder

Balder er valgt som revisor.

Der skal vælges to suppleanter til bestyrelsen, og evt. en revisorsuppleant.

I den forgangne periode har Karsten og Thomas været suppleanter til bestyrelsen. De ønsker begge gerne at fortsætte.

Karsten og Thomas er valgt som suppleanter til bestyrelsen.

Ingen stiller op til posten som revisorsuppleant.


## 7: Eventuelt

Vidir nævner at vi på [member.data.coop](https://member.data.coop/) har fået et meget simpelt medlemssystem op, hvor man kan oprette sig -- og indtil videre ikke meget mere end det. Men på sigt vil man også her kunne administrere betaling af sit kontingent.

Han siger også at man som medlem kan få en @data.coop-e-mailadresse, hvis man skulle have lyst til det.

Det er pt. ikke muligt at få en postkasse på andre domæner (fx sit eget), men det arbejdes der selvfølgelig på.

Mikkel og Jesper fortæller om at vi til arbejdsdagen i går havde besøg af David fra et andet datafællesskab med base i Svendborg. Det var rigtig spændende at høre om hvordan hinanden arbejder og snakke om hvordan vi kan samarbejde og hjælpe hinanden fremadrettet.

Rasmus gør opmærksom på at det vi laver går under begrebet "libre hosting", som vi bør undersøge nærmere.

Vi kender også til Fri Post i Sverige og en gruppe et sted i Jylland som vi skal have sagt hej til.

Der opfordres til at sige til, hvis man støder på andre datafællesskaber.

Standarddagsordenen trænger til smårettelser hist og her.

Slut på eventuelt. Forpersonen runder af og takker for en god GF.
