# Generalforsamling 2021

## Metadata
**Dato:** 4. marts 2021

**Tidspunkt:** 20:00

**Sted:** Jitsi - annonceres til medlemmer via mail.

## Dagsorden

1. Valg af dirigent og referent.
1. Bestyrelsens beretning.
1. Fremlæggelse af regnskab, budget og kontingent.
1. Indkomne forslag.
    1. "Præcisering af hvor længe et medlemskab varer." https://git.data.coop/data.coop/dokumenter/pulls/11
    1. "Tilføjelse af paragraf om udpegning af administratorer" https://git.data.coop/data.coop/dokumenter/pulls/5
1. Godkendelse af vedtægtsændringer og Acceptable Use Policy
1. Valg (Jf. § 3)
    
    Følgende ønsker genvalg til bestyrelsen:
    - Balder
    - Mikkel

    Til revisor opstiller:
    - Balder

    Der skal vælges to suppleanter til bestyrelsen, og evt. en revisorsuppleant.
1. Eventuelt


## Forslag

Forslag kan findes som "issues" i dette git repostory under mærkatet "GF2021" (https://git.data.coop/data.coop/dokumenter/issues?labels=23).

Forslag kan stilles ved at oprette et issue, eller sendes til bestyrelsen på [board@data.coop](mailto:board@data.coop).

Forslag skal være modtaget af bestyrelsen senest en uge før generalforsamlingen, altså d. 17. marts 2021.

## Vedtægtsændringer

Vedtægtsændringer kan findes som "pull requests" til dette git repository under mærkatet "GF2021" (https://git.data.coop/data.coop/dokumenter/pulls?labels=23).

Vedtægtsændringer kan stilles ved at oprette et pull request, eller sendes til bestyrelsen på [board@data.coop](mailto:board@data.coop).

Vedtægtsændringer skal være modtaget af bestyrelsen senest en uge før generalforsamlingen, altså d. 17. marts 2021.

## Spørgsmål

Har du nogen spørgsmål til generalforsamlingen kan du kontakte bestyrelsen på [board@data.coop](mailto:board@data.coop). Bestyrelsen er også at finde på vores samlede Matrix/IRC kanal som kan findes på #data.coop:data.coop (Matrix) og #data.coop @ freenode (IRC).