## Bestyrelsens beretning 2020-2021

Der er ikke gået lang tid siden sidste generalforsamling, men det betyder ikke at der ikke er sket noget!

### Hvad vil vi opnå i år?

Som noget nyt har bestyrelsen lavet en liste af opgaver som vi ser som det vi gerne vil opnå i 2021. Det havde selvfølgelig været nærliggende at opgaverne skulle løses inden næste generalforsamling, men grundet rykningen af denne er der tale om 2021 - og reelt nok indtil generalforsamlingen i 2022.

I ikke-prioriteret rækkefølge:

* [ ] **Ny hjemmeside** - Der er et design, og meget af arbejdet er gjort. Men vi skal skubbe det over målstregen (det foreløbige kan ses på https://new.data.coop)
* [ ] **Medlemssystem** - Det skal være nemmere at blive medlem og betale kontingent.
* [ ] **Mailinglister** - Vi vil gerne have vores kommunikation ind i mailinglister - og muligvis tilbyde vores medlemmer mailinglister på sigt.
* [x] **Backup** - Før vi rigtig kan begynde at stole på vores services skal vi have en ordenlig backupløsning
* [ ] **Procedurer for services** - Vores medlemmer skal have adgang til vores services, også selvom det kræver en manuel proces at opsætte brugere. Det skal vi gøre så nemt som muligt.
* [ ] **Onboarding/offboarding af administratorer** - Der skal være faste procedurer for de folk der bliver udpeget til at være administratorer, samt hvis disse stopper.
* [ ] **Mindst 30 kontingentbetalende medlemmer**

Det er bestyrelsens håb at vi fremadrettet kan opstille lignende mål for at sikre fremdrift.

### Arbejdsmøder

For at skabe mere medlemsinvolvering har vi afholdt arbejdsmøder hver anden tirsdag - med start d. 26. januar.

Det er bestyrelsens opfattelse at det har været en fin måde for medlemmer at mødes, og der er da også blevet udrettet ting. Der har dog nok været mangel på en struktureret oversigt over opgaver, især lavthængende opgaver.

Mange af vores identificerede opgaver er af en teknisk karakter. Her kunne det være givtigt at udforske de ikke-tekniske aspekter af foreningens virke. Dette kan være alt fra forefaldende foreningsarbejde til propagandaproduktion og idéudvikling. 

### Bankskifte

Vores udgifter til bank er uforholdsvis høje i forhold til vores egentlige behov. Derfor har bestyrelsen undersøgt billigere alternativet, hvilket også har været oppe på tidligere generalforsamlinger. Valget er faldet på Folkesparekassen.

Der skal stilles en garanti på 5000kr, hvilket foreningen i skrivende stund ikke har. Vi er derfor så heldige at der er en venlig sjæl der har indvilliget i at stille denne garanti på vores vegne.

Desuden kræver en oprettelse af sådan en konto at enten forperson eller kasserer er kunde hos Folkesparekassen. Vi har heldigvis et medlem af bestyrelsen der er kunde hos Folkesparekassen og er parat til at påtage sig kassererposten.

Bankskiftet kan igangsættes efter denne forsamlings afslutning.

### Medlemsfremgang og ny struktur for kontingentbetaling

Vi har 24 aktive kontingentbetalende medlemmer, hvilket lover godt for både opbakningen og potentialet for vores videre aktiviteter.

I opløbet til denne generalforsamling har vi diskuteret og stillet et forslag til, at medlemskab fremover løber mellem de ordinære generalforsamlinger. Motivationen kommer vi ind på senere.

Men vi kan allerede nu konstatere, at vi er glade for at kunne afvikle vores generalforsamling langt tidligere på året og dermed frigøre tiden og fokus på arbejdet i foreningen.