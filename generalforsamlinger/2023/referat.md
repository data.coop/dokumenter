# Referat af Generalforsamling 2023

## Tid og sted

København (online) d 22. juni 2023

## Deltagere

* Vidir Valberg Gudmundsson
* Mikkel Munch Mortensen
* Benjamin Balder Bach
* Sam Al-Sapti
* Karsten Højgaard
* Reynir
* Sune
* Rasmus
* halfd
* Thomas Hansen

Brug fulde navne, med mindre folk ønsker anonymiseret alias.

## Valg af dirigent og referent

* Reynir er referent
* Vidir er dirigent

## Dagsorden (generalforsamling)

1. Valg af dirigent og referent.
1. Bestyrelsens beretning.
1. Fremlæggelse af regnskab, budget og kontingent.
1. Indkomne forslag.
1. Godkendelse af vedtægtsændringer og Acceptable Use Policy
1. Valg (Jf. § 3)
1. Eventuelt

### Valg af dirigent og referent

Vidir blev valgt som dirigent, og Reynir valgt som referent. Der blev gjort opmærksom på at man ikke kan være referent eller dirigent hvis man er på valg.

### Bestyrelsens beretning

Der har været opture og nedture. Det største har været at vi mistede graffen. Gulvet er åbent for et par ord om graffen. Balder foreslår at vi mødes eks. ved årsdagen.

Beretningen kan findes her: https://git.data.coop/data.coop/dokumenter/pulls/31/files

Sam kan fortælle at backup virker.

Rasmus spørger ind til donationen.

### Fremlæggelse af regnskab, budget og kontingent

#### Regnskab

Regnskabet bliver fremlagt af Balder. At bemærke var at pga. el-priser blev vi faktureret for server hosting, og banken krævede underskrifter.

Der bliver afsløret at det er Thomas "tux" Hansen der donerede beløbet.

Der snakkes om bankgebyrer og bankskift. Vi har Merkur Bank som er dyr for foreninger. Bestyrelsen har undersøgt andre banker og forsøgt bankskifte uden held. Arbejdernes Landsbank bliver foreslået.

Ingen indsigelser til regnskabet. Regnskabet er godkendt.

#### Budget

Et budget bliver live coded 👨‍💻🧮

Harddiske til 16.000 kroner og klistermærker til 1.500 kr bliver tilføjet til budgettet.

#### Kontingent

Spørgsmål om tidspunkt og gyldighed for kontingentbetaling.

Slut på budget.

En forvirrende linje i budget blev fjernet.

#### Kontingent, this time for real

Servicegebyr(?) og detaljer bliver diskuteret. Der er ikke nogen konkrete forslag. Bestyrelsen vil diskutere det videre og stillet foreslag til næste generalforsamling.

Der er forslag til at beholde kontingentsatser som hidtil, og et andet forslag at hæve det almindelige kontingent fra 300 kr til 350 kr, og beholde studenterkontingentet á 50 kr.

Der bliver afstemt om vi skal hæve standard kontingentet til 350 kr. Afstemningen blev 5 for og 5 imod. Da der ikke er simpelt flertal for blev det ikke vedtaget.

Forslag om at beholde kontingent som hidtil blev vedtaget.

#### Godkendelse af budget

Budgettet blev delt igen.

Ingen indvendelser. Vidir ~~merger~~ rebaser og fast forwarder budgettet i https://git.data.coop/data.coop/dokumenter/pulls/32

### Indkomne forslag

Notits: Vi har ikke været gode nok til at fremhæve hvor forslag kan tilgås ud til medlemmer forud for generalforsamlingen. Vi indstiller til at vi gør det bedre næste gang.

- Ændring af flertalskrav ved foreningens opløsning https://git.data.coop/data.coop/dokumenter/pulls/23

- Forslag til Mastodon modereringspolitik og håndbog for moderatorer: https://git.data.coop/data.coop/dokumenter/pulls/28

- RIP Freenode. Long live libera.chat!
https://git.data.coop/data.coop/dokumenter/pulls/18

- Nedlæg social.data.coop https://git.data.coop/data.coop/dokumenter/issues/27

### Godkendelse af vedtægtsændringer og Acceptable Use Policy

#### Opdatering af acceptable usage policy

Der bemærkes at AUP'en bør omformuleres så opdatering af kommunikationskanaler ikke kræver en generalforsamling.

Forslaget bliver vedtaget uden indvendinger.

#### Ændring af flertalskrav ved foreningens opløsning

Forslaget er forkastet med 1 for og 8 imod. Vidir lukker pull requestet.

#### Forslag til Mastodon modereringspolitik

Balder fremlægger foreslaget.

Vi har en AUP som står øverst, men er ikke tilstrækkelig som Mastodon modereringspolitik.

Sune gaber.

*Referent skifter til Rasmus*

Forslaget om modereringspolitik var ikke udsendt med indkaldelsen, så kunne ikke behandles. Det blev bekræftet ved afstemning (5/4). Efter afstemningen blev der flyttet en stemme. Det blev besluttet at det ikke var afgørende.

Bestyrelsen nedsætter en arbejdsgruppe til udarbejdelse af modereringspolitik

### Valg (Jf. § 3)

Mikkel og Halfdan var på valg. Begge genopstillede.

Der var endnu en plads. Balder og Sune stillede op. Sune trak sit kandidatur. De øvrige blev valgt uden valghandling.

Som suppleanter stillede Sam og Sune op. De blev valgt.

Som revisor blev Karsten valgt.

### Eventuelt

Domænet riot.data.coop (til Element) bliver fjernet om ca. en måned, og bliver erstattet af et redirect til element.data.coop (som allerede eksisterer).
