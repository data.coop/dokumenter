# Bestyrelsens beretning for 2022-2023

Det forgangne foreningsår har budt på op- og nedture.

I oktober døde Jesper. Det var et stort personligt tab for en del medlemmer, der også kendte ham i andre sammenhænge end data.coop. Men derudover et stort tab for foreningen. For Jesper havde været en af de drivende kræfter siden vi senest genstartede foreningen. Både i bestyrelsen, men også i alt det praktiske med drift og udvikling af vores infrastruktur. Jesper er savnet og vil aldrig blive glemt. Som vi skrev i den mindebesked vi satte på forsiden af vores hjemmeside: data.coop fortsætter i dit minde og i din ære.

Heldigvis har året derudover primært budt på opture.

Et venligt medlem har generøst doneret et ikke ubetydeligt beløb til os, til indkøb af diske til backup. Vi mangler stadig at føre dette ud i livet.

Apropos backup, så lå vores backup indtil oktober hjemme hos Jesper. Vi fik den midlertidigt flyttet hjem til Mikkel, men har nu fået et bedre setup, hvor den sendes hjem til Fedder.

Ved seneste generalforsamling havde vi netop lanceret Mastodon som tjeneste til medlemmerne. Vi har nu haft det kørende det første fulde foreningsår, og det lader til at være taget godt imod af de medlemmer der har valgt at bruge tjenesten. Dog har det også medført nogle nye problematikker for foreningen, som vi ikke tidligere har været stillet overfor, i form af indholdsmoderering. Både i praksis at få det gjort, og finde ud af hvem der skal gøre det, men især også dilemmaerne og diskussionerne om hvad der skal modereres og hvordan. Dette er noget der skal arbejdes videre med i det kommende år, da det står klart at vores AUP ikke er tilstrækkelig her.

Gennem Mastodon er vi blevet opmærksomme på en række andre datafællesskaber rundt omkring derude. De griber alle tingene lidt forskelligt an, men grundlæggende deler de værdier med os. Det er opmuntrende, og det kunne være interessant på et tidspunkt at prøve at organisere fællesskaberne i et større fællesfællesskab, for at stå stærkere og lære af hinanden.

I november lancerede vi endnu en tjeneste, Rallly, der kan bruges til at koordinere aftaler (a la Doodle før det blev ødelagt). Vi valgte at gøre det til en tjeneste der er åben for alle – også ikke-medlemmer.

Vi har holdt 2 “Hej!”-arrangementer i løbet af året. Det første i januar var velbesøgt og blev en hyggelig mulighed for at hilse på hinanden ansigt til ansigt, diskuture hvad vi vil med foreningen og endte med forskelligt praktisk arbejde. Den anden “Hej!”-dag i maj var desværre knapt så velbesøgt. Men det er forhåbentligt noget vi kommer til at gøre igen fremover.

Hen over året er nye medlemmer dryppet ind. Og vi har her ved udgangen af foreningsåret 34 betalende medlemmer. Derudover er der mørketallet, der fortsat skyldes manglende overblik, grundet vores ikke-eksisterende medlemssystem.

Alt i alt går det stille og roligt fremad. Og vi håber at Jesper sidder et sted og glædes ved at vores forening trives.