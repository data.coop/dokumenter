# Referat af Bestyrelsesmøde

## Tid og sted

Internet, 25. september 2023

## Referat fra forrige bestyrelsesmøde

https://pad.data.coop/tFb7V6VJQS-VoecL25Zdaw?view

## Deltagere

* Vidir Valberg Gudmundsson
* Mikkel Munch Mortensen
* Halfdan Mouritzen
* Benjamin Balder Bach
* Sam Al-Sapti (Reynir er på pause fra bestyrelsen)


## Valg af dirigent og referent

* X er referent
* Y er dirigent

## Dagsorden

1. Valg af referent og dirigent
2. Moderationspolitik og ekstra GF
3. Bankskifte
4. Ny hardware-opsparingskonto
5. Medlemssystem
6. Opdeling af kontingent i servicegebyr og kontingent
7. Indkøb af diske
8. Administration af @datacoop på social.data.coop
9. @data.coop på Twitter
10. Hej-dage
11. Brug af den anden server

---
### 1. Valg af dirigent og referent

Dirigent: Mikkel

Referent: Balder

---
### 2. Moderationspolitik (MP) og ekstra GF

Jeg (Mikkel) blev (til min egen store overraskelse) udpeget som tovholder for arbejdsgruppen. Ingen reagerede på den invitation der blev sendt ud på e-mail. Da jeg sidst tjekkede, var dem der dukkede op i det dertilindrettede Matrix-rum næsten 1:1 bestyrelsen (en enkelt anden var derinde, et enkelt bestyrelsesmedlem var fraværende).

Mit forslag er at vi bare fortsætter arbejdet med MP i bestyrelsesregi og husker at melde ud i Matrix når der er noget at melde ud, så vi følger den proces vi selv har lagt op til. Lad os tage udgangspunkt i det arbejde nogle af jer allerede havde lagt i forslaget der blev stillet på seneste GF.

**Beslutninger/Action items:**

- Mikkel sætter gang i Matrix-rummet

---
### 3. Bankskifte

Hvordan får vi det eksekveret?

**Beslutninger/Action items:**

- Balder lover at gøre noget snart!

---

### 4. Ny hardware-opsparingskonto

Ifm. fremtidige donationer, foreslås det (Balder), at vi har en øremærket konto til fremtidige indkøb.

Reservens størrelse kan være en kombination af:

1. Nødvendig tilsidesættelse af midler til vedligeholdelse eller ved uforudsete hændelser
2. Donationer til forbedringer og ny-investeringer

Ideen med at have en separat konto er:

1. Medlemmer, som donerer, kan være garanteret, at deres midler anvendes til formålet.
2. Bestyrelsen og generalforsamlingen har et konstant overblik over det økonomiske sikkerhedsnet, som foreningen råder over.
3. Modellen kan udvides til andre områder med øremærkede midler: F.eks. fondsmidler, markedsføring og sysadmin-honorar


**Beslutninger/Action items:**

- Kontoplan:
  - Panik-konto
  - Kontingentkonto
  - Driftskonto
  - Øremærkede midler
- Næste GF bør have diskussion af hvor mange midler vi vil hensætte til panikkontoen.

---
### 5. Medlemssystem

Vidir har testet byro, og er kommet frem til at det ikke giver mening at bruge det fremfor vores eget.

Vi lægger vægt på, at det nok er data.coop's største prioritet i 2023 at komme videre med det her!

**Beslutninger/Action items:**

- Vi skal lave en plan for hvad der mangler at blive lavet.
- Vi vil gerne have et system hvor bestyrelsen kan nå at "vette" nye medlemmer inden de får "lov til at betale".
- Vi skal sørge for at det bliver et nice flow at blive medlem.
- **Vidir** laver en rallly om en dag hvor vi ser på hvad der ligger nu og hvad det er vi gerne vil have som minimum.

---
### 6. Opdeling af kontingent i servicegebyr og kontingent

v/ Balder & Víðir

* Opdeling i individuelle services vs. almenyttige services udstillet til hele internettet.
* Mulighed for at betale mere direkte for services i stedet for medlemsafgift.
* Vigtigt at indføre understøttelse af service-gebyr gennem vedtægter og GF.
* Vi kan opstille en basispakke, som vi kan diskutere videre. Det er vigtigt ikke at starte med en masse diskusioner om hvad individuelle services koster.

Nøglebegreber:

* Kontingent (momsfrit)
* Serviceafgift (moms!)

**Beslutninger/Action items:**

- Der skal skrives et forslag til GF der forklarer idéen og retter i vedtægterne i de rigtige paragraffer.
- Vi skal finde ud af hvordan vi beregner hvor meget ressource der bliver brugt på individuelle services vs. almenyttige services. **Halfdan** ser på at finde på en måde at lave udregningen.
- **Balder** kontakter Skattestyrelsen og hører hvad deres holdning er om situationen.


---
### 7. Indkøb af diske samt geninstallering af server

Halfdan og Vidir har kigget på diske.

https://pad.data.coop/coWlezr3SAqlz1SqHUHi-w?both

Sams noter:
https://pad.data.coop/lHv6HqjsSmG_6TF4ttHPyw?both#

Vi skal købe diskene.

Sam fortæller om Proxmox + ZFS + Consumer SSD'er er dårligt sammen i følge en erfaring. Flaskehalsen ligger i IOPS (internal operation) på disken. Hvis man vil have RAID + ZFS + Redudancy kræver enterprise, kan give op til en 10x bedre performance på IOPS.

Vi skal planlægge en dag hvor vi tager serveren helt offline for at installere et nyt system med virtuelle maskiner osv.

Vi skal bruge flere ipv4 adresser til det nye setup, nærmere bestemt 3 nye. Vi har én lige nu, og det koster 15kr om måneden per ip adresse - dvs. 45kr om måneden for tre ekstra ip'er (540kr om året)


**Beslutninger/Action items:**

- Sysadmins beslutter diskene og dokumenterer beslutningen.
- Vi mangler et kreditkort p.t. så nogen laver et udlæg og bliver refunderet.

### 8. Administration af @datacoop på social.data.coop

Forslag til procedure:

Alle fra bestyrelsen, som vil have adgang til kontoen, kan få det. At toot'e indebære at få mindst 1 "thumbs up" i Signal-gruppen til sit draft.

(Selvfølgelig med mindre toot'et indeholder holdninger eller beslutninger, som hele bestyrelsen skal ind over)

**Beslutninger/Action items:**

- Ingen holdninger, bare vidensdeling: Man behøver ikke vente på resten af bestyrelsen
- Holdninger, som ikke er kontroversielle: Del først i bestyrelses-signal og vent på thumbs up
- Holdninger, som ikke er afstemt: Vent på hele bestyrelsen

---
### 9. @datacoopdk på X

https://twitter.com/datacoopdk/verified_followers

Muligheder:

1. behold kontoen, benyt samme procedure som på Mastodon
2. frys kontoen og henvis til Mastodon
3. luk kontoen helt

**Beslutninger/Action items:**

- Vi opreklamerer open source alternativer til Twitter/X med den risiko, at kontoen lukkes.
- Alternativt, lukker vi kontoen, hvis vi ikke får os selv blokeret.

---
### 10. Hej-dage

**Beslutninger/Action items:**

- Flyt til kl 13, så vi har mere tid.
- Husk at sende påmindelser ud 2 uger før + et par dage før

- De næste gange:
  - November 2023 - lørdag d. 4. november
  - Februar 2024 - lørdag d. 3. februar
  - Maj 2024 - lørdag d. 4. maj

- Halfdan booker
- Balder sender ud

---
### 11. Brug af den anden server

Skal vi overveje at tage den anden server i brug?

Vi kunne give medlemmer mulighed for at få en VPS ala https://exozy.me/about og https://the-dam.org/

**Beslutninger/Action items:**

- Vi skrinlægger idéen for nu - nu er den blevet luftet

### 12. Eventuelt eventuelt?

Flere bestyrelsesmøder -  **Vidir** indkalder til næste møde! 📣
