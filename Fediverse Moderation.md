# data.coop moderation på Fødiverset

Denne politik forstås primært som vores politik for moderation af indhold på vores Mastodon-instans, men henvender sig også til øvrige fremtidige tjenester, som bliver drevet via ActivityPub-protokollen.

Tilsammen forstås både vores Mastodon-server og alle andre ActivityPub-servere som en del af "Fødiverset" (Fediverse på engelsk). ActivityPub-servere omtales som "instanser" i Fødiverset.

Indholdet, som modereres:

1) Indhold udgivet af medlemmerne via data.coops egen instans.
2) Indhold fra andre instanser, som medlemmerne abonnerer på og som dermed kommer til at blive delt via vores instans.

## Vision for en Mastodon-instans

I 2022 skete et stort ryk mod decentraliserede sociale medier, specielt platformen Mastodon. Derfor oplevede data.coop en del henvendelser på baggrund af vores test-instans social.data.coop.

Som forening tror vi på, at en fødereret vision for sociale medier kan lade sig gøre, hvis organisationer løfter udfordringerne med moderation og misinformation på en seriøs måde. data.coop tror dermed IKKE på uendelig ytringsfrihed - dvs. ingen frihed uden ansvar. Vi ønsker en debat, hvor deltagere respekterer hinanden, og hvor alle mennesker kan være frie fra forfølgelse og trusler, som vi desværre ser er en del af hverdagen på de store kommercielle medier.

Det danske landskab tæller allerede en del instanser, hvor data.coop ser ud til at gøre sig bemærket på at være betalt af medlemmerne, dvs. at man skal betale for medlemskab af data.coop for at kunne benytte Mastodon-instansen. Vi ser dette som en interessant model, som flere organisationer kunne følge, f.eks. fagforeninger eller andre interesseorganisationer med medlemmer.

I data.coops første år (2022-2023) med en Mastodon-instans, har flere medlemmer som en del af moderator-holdet udtrykt ønske om at have et demokratisk vedtaget sæt af regler for moderation og en håndbog for håndhævelse af reglerne.

For at kunne fortsætte med at drive social.data.coop, ser vi derfor to muligheder:

1) At vedtage regler for og håndhævelse af moderation på vores Mastodon-instans.
2) At lukke instansen, fordi moderation ikke er muligt uden et mandat fra generalforsamlingen.

## Regler for brugere

data.coops Mastodon-instans varetages af et hold moderatorer, som reagerer på henvendelser om indhold, der både kan være fra andre data.coop-medlemmer eller fra andre brugere og instanser på netværket.

Henvendelser behandles fra både data.coops medlemmer og andre brugere på netværket.

Reglerne for alle henvendelser:

* Acceptable Usage Policy, som også gælder for social.data.coop: https://git.data.coop/data.coop/dokumenter/src/branch/main/Acceptable%20Usage%20Policy.md
* Do not engage in homophobic, racist, transphobic, ableist, sexist, or otherwise prejudiced behaviour.
* Do not harass people. Stalking, unconsented contact, or unwanted sexual attention is harassment.
* Aggression and elitism are not welcome — nobody should be afraid to ask questions.
* Spam and tracking: Content that is directly aimed advertising commercial products or contains tracker links can be considered spam. Content containing third-party tracker links will be removed. Promoting content by mentioning users that are not following you is considered spamful behavior and the user targeted may report this.
* Unconsented collection and usage of data is not allowed. Sharing and usage of data that falls outside of the ActivityPub protocol and what can be considered normal Fediverse usage will be considered unconsented. Moderators and system admins retain their rights to immediately block instances and profiles that engage with such activities.

Bemærk at alle medlemmer har pligt til at gøre sig bekendte med data.coops Acceptable Usage Policy.

Vi har skrevet reglerne på engelsk, da det er vigtigt, at især andre instans-moderatorer i Fediverset kan se, hvad der gælder for vores instans.

## Oplever du noget som uønsket adfærd?

data.coops moderator-hold er klar til at fjerne indhold og blokere brugere fra vores instans.

Indrapportér altid indhold, som du ikke ønsker, og som er et brud på reglerne.

Du må meget gerne angive, hvilken regel du mener bliver brudt, hvis det kan virke uklart - på den måde kan en moderator spare lidt tid.

Hvis nogen henvender sig til dig uden dit ønske, så sig det tydeligt: "Jeg er desværre ikke interesseret i at fortsætte debatten om dette emne". Hvis en bruger ikke respekterer dit ønske, så må vi anse det som uønsket kontakt eller i værste tilfælde stalking.

## Tekniske begrænsninger

Moderatorer og sysadmins beslutter i fællesskab indstillinger for serveren såsom:

* Max upload størrelse
* Max antal tegn
* Opdateringer til nye Mastodon-versioner

## Deling af indhold med andre instanser

*Dette afsnit kan omtales "Meta-reglen", da det er skrevet med særligt henblik på Metas implementering af ActivityPub til Threads.net og Instagram.*

Bestyrelsen kan tage stilling til at blokere for deling med ActivityPub-instanser med udgangspunkt i ejerskabet af instanserne. Bestyrelsen foretager en vurdering af, om en eller flere af disse kritierier er opfyldte:

* At der findes modstridende interesser mellem ejeren og data.coop.
* At ejerens aktiviteter udgør en eksistentiel trussel for en eller flere af data.coops services.
* At der findes en seriøs mistillid til ejerens evne til at moderere indhold i henhold til vores AUP eller modereringspolitik.
* At der er mistanke om, at ejeren uden samtykke kommer til at anvende medlemmernes AcitvityPub-data, indsamlet gennem fødereringen eller på anden vis, på en måde, der påkræver samtykke.

Beslutningen betyder, at ingen profiler på data.coops ActivityPub-server kan følges fra omfattede servere og betyder, at ingen profiler på omfattede servere kan følges fra data.coops ActivityPub-server.

En protokol for beslutninger vedligeholdes i denne politik.

## Håndbog for moderatorer

Moderatorer kan gøre følgende:

### Fjerne indhold

Indhold, som deles direkte fra en data.coop-konto kan slettes af moderatorer.

Dette vil altid gøre sig gældende, når indholdet er i strid med reglerne.

### Skjule profiler

Profiler, som udviser en adfærd, der klart er til gene for brugere af data.coops instans, kan skjules. Der bør dog udvises maksimalt hensyn til, når enkelte brugere ikke er enige.

Hvis der er tale om profiler, der ikke overholder reglerne, bør disse blokeres.

Det er kun ved klare overtrædelser, at dette forhold afviges.

### Blokering af profiler fra andre instanser

Profiler fra andre instanser, blokeres såfremt de ikke overholder vores regler.

### Blokering af instanser

Hvis en instans udviser en adfærd, som ikke er forenelig med vores regler, kan moderatorer vælge at blokere instansen.

### Transparens og skabeloner

Proaktiv blokering af instanser:

> Vi er informeret om at instansen <domæne> deler <årsag-om-indhold>. Moderationen har derfor taget initiativ til at blokere instansen og dermed sikre os mod, at vi ikke fødererer data med den.


### Moderatorholdets metode og mandat

Som udgangspunkt skal moderation foregå under moderationsholdets diskretion, dog skal der findes en log.

* Moderator-holdet bør altid bestå af mindst 3 medlemmer.
* En moderator kan selv behandle en indrapportering.
* Alle sager skal behandles via indrapporteringer af indhold, profiler eller instanser.
* En moderator kan på egen hånd vælge at lave en indrapportering.
* Diskussioner internt i moderatorholdet foregår i et lukket chatrum, hvor historikken arkiveres i op til 1 år.

### Handlinger overfor data.coops egne medlemmer

#### Inden sanktioner

Hvis en bruger overtræder reglerne, henvender moderator-gruppen sig via en fælles profil til brugeren med en anmodning om at fjerne indholdet og f.eks. fremvise en forståelse for dette offentligt eller på anden måde genskabe respekt og forståelse fra evt. udsatte parter.

Dette skal også ses i lyset af, at flere andre data.coop-medlemmer har profil på samme instans, og at der kan opstå en reel tvivl om data.coops formål og hensigter, hvis regler brydes uden et synligt efterspil.

#### Advarsler

Hvis en af data.coops egne medlemmer bryder reglerne, skal moderatoren som udgangspunkt give en advarsel.

#### Suspendering

Hvis et medlem udviser gentagende brud på regler eller et enkeltstående særligt groft brud på reglerne, kan moderatorerne lukke profilen.

Suspendering af et medlems Mastodon-profil bør føre til at brugerens overholdelse af AUP granskes af data.coops bestyrelse, og moderatorholdet skal hurtigst muligt dele sine overvejelser.

## Inspiration

Denne politik er blevet til gennem diskussioner blandt medlemmerne i data.coop, men herunder også med inspiration fra andre moderationspolikker:

* https://info.eldritch.cafe/moderation-guidelines/
* https://starshipgender.com/cultivating-community
* http://weirderearth.de/moderator_guidelines.html

## Beslutningsprotokol

### 2025-01-12: # Beslutning ang. (de)federering med Meta-ejede servere

*Vedtaget enstemmigt i bestyrelsen 12. januar 2025*

#### Baggrund

Det har – siden Meta annoncerede at Threads.net skulle forbindes til "Fødiverset" gennem ActivityPub – været ønsket fra flere i foreningen, at data.coop tager generel stilling til, om vi fortsat vil tillade, at vores server(e) (p.t. social.data.coop) udveksler data med Meta/Threads, eller om vi skal blokere udvekslingen. Pga. omfattende spørgsmål og bekymringer om Metas moderationspolitik har vi kigget nærmere på sagen, som har fået fornyet motivation tidligt i 2025, da Meta besluttede at:

- Droppe deres nuværende moderation og omtaler X-platformen (tidl Twitter) som et forbillede.
- Løsne op for reglerne om hadtale og dermed bevæge sig i retning (endnu længere) væk fra vores egne regler og AUP.

#### Beslutningskriterier

Med henvisning til vores regel "at der findes en seriøs mistillid til ejerens evne til at moderere indhold i henhold til vores AUP eller modereringspolitik.":

* Seneste udtalelser fra Mark Zuckerberg om fremtiden for moderation på Metas platforme i 2025 [1]
* Omfattende negligering af ansvar ved folkedrab og krænkelser af menneskerettigheder på Metas platforme, som beskrevet af Amnesty i 2023 [2] [3] [4]
* Overlagt negligering af ansvar for/medvirken til statslige aktøres manipulation og omfattende misinformationskampagner, som dokumenteret af flere whistleblowers [5] [6] [7]
* Metas censur af politisk indhold, hvor vi opfatter, at Meta ville censurere indhold delt af data.coops medlemmer, som beskrevet af Human Rights Watch i 2023 [8]

Med henvisning til "At der findes modstridende interesser mellem ejeren og data.coop.":

* At Meta generelt arbejder for en centraliseret, kommerciel anvendelse og indsamling af brugerdata i strid med data.coops formål og principper.

Med henvisning til "At ejerens aktiviteter udgør en eksistentiel trussel for en eller flere af data.coops services.":

* Der findes begrundet mistanke for at Threads deltager i Fødiverset enten for at lokke brugere over på deres platforme eller udgøre en dominerende magtfaktor. Flere medlemmer har udtrykt bekymring for "embrace, extend, extinguish"[9] scenarier.

Med henvisning til "At der er mistanke om, at ejeren uden samtykke kommer til at anvende medlemmernes AcitvityPub-data, indsamlet gennem fødereringen eller på anden vis, på en måde, der påkræver samtykke."

* Nuværende AI-teknologier trænes uden samtykke på åbent indhold. Fordi ActivityPub-protokollen ikke muliggør en klar definition for samtykke omkring anvendelse af data, og fordi vi allerede ved, at AIer trænes i modstrid til gældende lov om ophavsret, ser vi det også som sandsynligt, at data fra data.coops brugere kan overføres via ActivityPub til Threads og dermed f.eks. bruges til AI-træning eller anden privatlivskrænkende profilering af medlemmer.

#### Beslutning

Bestyrelsen er enige om, at der samlet set er gode nok argumenter for at blokere for Metas ActivityPub-platforme, i første omgang konkret Threads.net. Nogle argumenter er nok i sig selv, og sammenlagt udgør de et klart billede af, at vi ikke ønsker:

* Vores medlemmers data udvekslet med Meta/Threads.
* Indhold fra Meta/Threads udvekslet via vores servere.

***Vi beslutter derfor at blokere al ActivityPub-baseret udveksling af data med Meta og Threads.net. Herunder også andre potentielle, fremtidige ActivityPub-baserede tjenester fra Meta.***


#### Kilder


[1] https://www.dr.dk/nyheder/viden/teknologi/i-2008-var-zuckerberg-en-stor-undskyldning-det-er-han-langtfra-i-dag

[2] https://www.amnesty.org/en/latest/news/2023/08/myanmar-time-for-meta-to-pay-reparations-to-rohingya-for-role-in-ethnic-cleansing/

[3] https://www.amnesty.org/en/latest/news/2023/10/meta-failure-contributed-to-abuses-against-tigray-ethiopia/

[4] https://erinkissane.com/meta-in-myanmar-full-series

[5] https://www.theguardian.com/technology/2021/apr/12/facebook-fake-engagement-whistleblower-sophie-zhang

[6] https://en.wikipedia.org/wiki/Frances_Haugen

[7] https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal

[8] https://www.hrw.org/report/2023/12/21/metas-broken-promises/systemic-censorship-palestine-content-instagram-and

[9] https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish


