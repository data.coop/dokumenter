# Referat af <Bestyrelsesmøde | Generalforsamling>

## Tid og sted

København, 1. januar 2023

## Deltagere

* Bestyrelsesmedlem 1
* Bestyrelsesmedlem 2
* Bestyrelsesmedlem 3
* Bestyrelsesmedlem 4
* Bestyrelsesmedlem 5
* Bestyrelsesmedlem 6

Brug fulde navne, med mindre folk ønsker anonymiseret alias.

## Valg af dirigent og referent

* X er referent
* Y er dirigent

## Dagsorden (generalforsamling)

1. Valg af dirigent og referent.
1. Bestyrelsens beretning.
1. Fremlæggelse af regnskab, budget og kontingent.
1. Indkomne forslag.
1. Godkendelse af vedtægtsændringer og Acceptable Use Policy
1. Valg (Jf. § 3)
1. Eventuelt

### Valg af dirigent og referent

...

### Bestyrelsens beretning

...

### Fremlæggelse af regnskab, budget og kontingent

...

### Indkomne forslag

...

### Godkendelse af vedtægtsændringer og Acceptable Use Policy

...

### Valg (Jf. § 3)

...

### Eventuelt

...
