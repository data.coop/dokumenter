# Medlemsydelse

*Senest opdateret 2024-06-01*

Jf. vedtægterne, definerer og prissætter bestyrelsen de ydelser, som medlemmerne indbetaler for at få adgang til tjenester.

Eftersom der er tale om en ydelse og samling tjenester, som kun er rettet mod medlemmer, taler vi om en medlemsydelse og pakke af medlemstjenester.

## 1. Priser

### 1.1. Medlemsydelsen

Alle medlemmer i data.coop betaler en medlemsydelse, som giver adgang til alle medlemstjenester. Dvs. alle tjenester, som ikke er åbne men derimod kræver et login.

Medlemsydelsen afregnes pr. måned:

- Medlemsydelse: 450 kr / år
- Medlemsydelse m. nedsat kontigent: 50 kr / år

*Alle priser er inklusive moms.*

## 2. Betalingsbetingelser

### 2.1 Betalingsbetingelser

Betaling sker med betalingskort gennem medlemssystemet.
Hvis der anvendes bankoverførsel, bedes medlemmet sende en ekstra notifikationer til kasserer@data.coop, når betalingen er afsendt.

- Medlemsydelse følger kontingentåret og betales forud for en periode på 12 måneder. Kontingentåret løber fra og med juli, til og med juni.

- Ved betaling i indmeldelsesåret, betales forud fra og med indmeldelsesmåneden til og med juni.
  Dvs. hvis man melder sig ind i maj, betaler man medlemsydelse for 2 måneder.

- Betalingen skal ske inden for 14 dage efter fakturadato.

### 2.2 Forsinket eller udebleven betaling

Hvis betalingen ikke er modtaget inden for 14 dage,
vil foreningen forbeholde sig retten til at lukke for adgangen til de tjenester, som medlemsydelsen dækker.

## 3. Leveringsbetingelser

Eftersom data.coop er en forening, og modtageren af tjenester er medlemmer af foreningen, forventes medlemmet at påtage sig et demokratisk ansvar ift. forventelig levering af tjenester fra foreningen, mod at foreningens overskud investeres tilbage i foreningen i medlemmernes interesse.

Der er ingen retturret: Fungerer en tjeneste ikke efter medlemmets forventninger, refunderes ydelsen i udgangspunktet IKKE.
Til gengæld bør medlemmet selvfølgelig søge indflydelse, forbedringer eller kompensationer gennem medlemskanaler eller henvendelser til bestyrelsen.

### 3.1. Garanti og erstatning

Eftersom software er komplekst, og foreningens software er Open Source,
forventes medlemmer at søge konstruktiv dialog og udbedring af fejl,
fremfor erstatning, kompensation eller refusion.

- Foreningen er ikke erstatningspligtig overfor medlemmer ifm. tjenester.
- Foreningen stiller ingen generelle garantier på tjenester, såsom oppetid eller fejlfri data-backup.
- Foreningen henviser til individuelle tjeneste-badges på foreningens hjemmeside.

### 3.2. Undtagelser i returret: Foreningens ansvar

- Ved fuldstændig udebleven levering eller andre grove forsømmelser

## 4. Ændringer

Jf. vedtægterne, skal alle ændringer til betalings- og leveringsbetingelser varsles 30 dage inden de træder i kraft.

Ændringer motiveres, opsummeres og vises i fuldt omfang gennem et Pull Request på git.data.coop.
